provider "aws" {
	region = "${var.aws_region}"
}

resource "aws_elb" "elb" {
  name = "${concat(var.environment, var.service_name)}"
  subnets = ["${var.subnet1}"]
  security_groups = ["${var.elb_security_group}"]
  listener {
    instance_port = "${var.instance_port}"
    instance_protocol = "${var.instance_protocol}"
    lb_port = "${var.lb_port}"
    lb_protocol = "${var.lb_protocol}"
  }
  health_check {
      healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "${var.lb_healthcheck_target}"
    interval = 30
  }
  cross_zone_load_balancing = true
  idle_timeout = 300
  connection_draining = true
  tags {
    Name = "${concat(var.environment, var.service_name, "-elb")}"
    Environment = "${var.environment}"
    BalancesService = "${concat(var.environment, var.service_name)}"
  }
}

resource "aws_launch_configuration" "lc" {
  image_id = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  security_groups = ["${var.instance_security_group}"]
  key_name = "${var.key_name}"
	iam_instance_profile = "${var.iam_instance_profile}"
	user_data = "${var.userdata}"
}

resource "aws_autoscaling_policy" "asgp" {
  name = "${concat(var.environment, var.service_name)}"
  scaling_adjustment = "${var.scaling_number}"
  adjustment_type = "ChangeInCapacity"
  cooldown = "${var.scaling_cooldown}"
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
}

resource "aws_autoscaling_group" "asg" {
	provisioner "local-exec" {
		command = "sleep 200"
	}
	name = "${concat(var.environment, var.service_name)}"
	availability_zones = ["${var.az1}"]
	vpc_zone_identifier = ["${var.subnet1}"]
  max_size = "${var.max_nodes}"
  min_size = "${var.min_nodes}"
  force_delete = true
  load_balancers = ["${aws_elb.elb.name}"]
  launch_configuration = "${aws_launch_configuration.lc.name}"
  tag {
    key = "Name"
    value = "${concat(var.environment, var.service_name)}"
    propagate_at_launch = true
  }

  tag {
    key = "Environment"
    value = "${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key = "Service"
    value = "AWS-Brainstorm"
    propagate_at_launch = true
  }

  tag {
    key = "CostCenter"
    value = "SGHE0000071560"
    propagate_at_launch = true
  }

  tag {
    key = "Expiration"
    value = "20160513 22:22:43 UTC"
    propagate_at_launch = true
  }

}
