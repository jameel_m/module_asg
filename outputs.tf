output "elb" {
  value = "${aws_elb.elb.name}"
}

output "elb_zone_id" {
  value = "${aws_elb.elb.zone_id}"
}
