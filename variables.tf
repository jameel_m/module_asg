variable "aws_region" {
  default = ""
}

variable "environment" {
  default = ""
}

variable "service_name" {
  default = ""
}

variable "instance_type" {
  default = ""
}

variable "key_name" {
  default = ""
}

variable "ami_id" {
  default = ""
}

variable "az1" {
  default = ""
}

variable "az2" {
  default = ""
}

variable "az3" {
  default = ""
}

variable "iam_instance_profile" {
  default = ""
}

variable "min_nodes" {
  default = ""
}

variable "max_nodes" {
  default = ""
}

variable "desired_nodes" {
  default = ""
}

variable "scaling_number" {
  default = ""
}

variable "scaling_cooldown" {
  default = ""
}

variable "instance_security_group" {
  default = ""
}

variable "subnet1" {
  default = ""
}

variable "subnet2" {
  default = ""
}

variable "subnet3" {
  default = ""
}

variable "userdata" {
  default = ""
}

variable "lb_port" {
  default = ""
}

variable "lb_protocol" {
  default = ""
}

variable "instance_port" {
  default = ""
}

variable "instance_protocol" {
  default = ""
}

variable "lb_healthcheck_target" {
  default = ""
}

variable "elb_security_group" {
  default = ""
}

variable "r53_zone_id" {
  default = ""
}

variable "service_url" {
  default = ""
}
